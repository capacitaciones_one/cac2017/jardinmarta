package entidades;

import java.util.Date;



public class Alumnos {
    String nombre;
    String fechaNac;
    String sexo;
    String turno;
    String tel;
    String sala;
    Responsable datosPadre;
    Docente docente;

    public Alumnos(String nombre, String fechaNac, String sexol, String turno, String tel, String sala, Responsable datosPadre, Docente docente) {
        this.nombre = nombre;
        this.fechaNac = fechaNac;
        this.sexo = sexol;
        this.turno = turno;
        this.tel = tel;
        this.sala = sala;
        this.datosPadre = datosPadre;
        this.docente = docente;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getSexol() {
        return sexo;
    }

    public void setSexol(String sexol) {
        this.sexo = sexol;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public Responsable getDatosPadre() {
        return datosPadre;
    }

    public void setDatosPadre(Responsable datosPadre) {
        this.datosPadre = datosPadre;
    }

    @Override
    public String toString() {
        return "Alumnos{" + "nombre=" + nombre + ", fechaNac=" + fechaNac + ", sexo=" + sexo + ", turno=" + turno + ", tel=" + tel + ", sala=" + sala + ", datosPadre=" + datosPadre + ", docente=" + docente + '}';
    }

    
    
    
}
