package entidades;

public class Docente {
    String nombre;

    @Override
    public String toString() {
        return "Docente{" + "nombre=" + nombre + '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Docente(String nombre) {
        this.nombre = nombre;
    }
}
