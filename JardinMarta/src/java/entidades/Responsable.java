package entidades;

public class Responsable {
    String nombre;
    String dni;
    String parentezco;
    String tel;
    String mail;

    public Responsable(String nombre, String dni, String parentezco, String tel, String mail) {
        this.nombre = nombre;
        this.dni = dni;
        this.parentezco = parentezco;
        this.tel = tel;
        this.mail = mail;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getParentezco() {
        return parentezco;
    }

    public void setParentezco(String parentezco) {
        this.parentezco = parentezco;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Responsable{" + "nombre=" + nombre + ", dni=" + dni + ", parentezco=" + parentezco + ", tel=" + tel + ", mail=" + mail + '}';
    }
    
    
    
}
