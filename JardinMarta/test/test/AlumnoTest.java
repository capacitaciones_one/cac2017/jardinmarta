package test;

import entidades.Alumnos;
import entidades.Docente;
import entidades.Responsable;
import java.util.Date;

public class AlumnoTest {
    public static void main(String[] args) {
        System.out.println("[...]");
        
        
        //CREAMOS RESPOSNABLES
        Responsable responsable1 = new Responsable("Ana", "22.444.111", "La niñera", "4921-0404", "@lanani");
        Responsable responsable2 = new Responsable("Lara", "44.333.555", "La mama","49219191", "lamama@mamu.com");
        Responsable responsable3 = new Responsable("Tiago", "18.252.111", "El papa", "5124-1414", "elpapa@elpadre.com");
        
        //CREAR DOCENTES
        Docente docente1 = new Docente("Tiara");
        Docente docente2 = new Docente("Lorena");
        Docente docente3 = new Docente("Pia");
        Docente docente4 = new Docente("Sergio");
        
        System.out.println(docente1);
        //CREAMOS ALUMNOS
        Alumnos matricula3333  = new Alumnos("Martin", "2016", "M", "Mañana", "2101-1111", "Celeste", responsable1, docente1);
        System.out.println(matricula3333);
        System.out.println("[OK]");
    }
    
}
